%% MAP estimation for the vehicle position

%% Initialization
clear; close all; clc


%% For each K <- {1,2,3,4} repeat the following.
% Place evenly spaced K landmarks on a circle with unit radius centered at the origin. 
K = 1;
flag = 1;

while flag
    %% Set the true vehicle location to be inside the circle with unit radious centered at the origin.  
    centerX = 0; centerY =0;
    rc = 1;
    a = 2*pi*rand(1);
    r = sqrt(rand(1));
    w = 2*pi/360; 
    
    for i = 1:1:360
        x(i) = rc*cos(w*i) + centerX;
        y(i) = rc*sin(w*i) + centerY;
    end
    
    v_loc_x = rc .* r .* cos(a) + centerX;
    v_loc_y = rc .* r .* sin(a) + centerY;

    if K == 1
        k_loc_x(1) = 1;
        k_loc_y(1) = 0;
    elseif K == 2
        k_loc_x = [0 0];
        k_loc_y = [1 -1];
    elseif K == 3
        k_loc_x = [0 -1/sqrt(2) 1/sqrt(2)];
        k_loc_y = [1 -1/sqrt(2) -1/sqrt(2)];
    else
        k_loc_x = [0 0 -1 1];
        k_loc_y = [1 -1 0 0];
    end
    
    % Set measurement noise standard deviation to 0.3 for all range measurements.
    % Generate K range measurements according to the model specified above 
    range = repmat([v_loc_x; v_loc_y], 1, K) - [k_loc_x; k_loc_y] + 0.3;
        
    % (if a range measurement turns out to be negative, reject it and resample; all range measurements need to be nonnegative).
    if any(range < 0)
        flag = 1
    else
        flag = 0
    end
end

% Plot the equilevel contours of the MAP estimation objective for the range of horizontal and vertical coordinates from -2 to 2; 
% superimpose the true location of the vehicle on these equilevel contours (e.g. use a +mark),
% as well as the landmark locations (e.g. use a o mark for each one).
for i = 1:K
    rr(i) = sqrt(range(1,i)^2 + range(2,i)^2);
end

hold on;
% Fix the axis limits.
xlim([-2 2])
ylim([-2 2])
plot(v_loc_x, v_loc_y, '+', 'LineWidth', 2, 'DisplayName','True location of the vehicle');
plot(k_loc_x, k_loc_y, 'o', 'LineWidth', 2, 'DisplayName', 'landmark locations');
title(sprintf('Plot the equilevel contours of the MAP estimation for K=%g', K)); 
legend('Location','best'); 
xlabel('x');
ylabel('y');

plot(x, y, 'HandleVisibility','off');

for i = 1:K
    for j = 1:1:360
        x(j) = rr(i)*cos(w*j) + k_loc_x(i);
        y(j) = rr(i)*sin(w*j) + k_loc_y(i);
    end
    plot(x, y, 'HandleVisibility','off');
end
grid on;
hold off;


% Provide plots of the MAP objective function contours for each value of K.
% When preparing your final contour plots for different K values, 
% make sure to plot contours at the same function value across each of the different contour plots 
% for easy visual comparison of the MAP objective landscapes.
% Suggestion: For values of sx and sy, you could use values around 0.25 and perhaps make them equal to each other. 
% Note that your choice of these indicates how confident the prior is about the origin as the location.
x = linspace(-2,2);
y = linspace(-2,2);

sigmaX = 0.25; sigmaY = 0.25;
sig = inv([sigmaX^2 0;0 sigmaY^2]);
aaa = [x; y];


prior = -1/2 * [x y] .* [sigmaX^2 0;0 sigmaY^2] .* [x y]';

prior = 1/(2*pi*sigmaX*sigmaY)*exp(-0.5 * [x y] .* inv([sigmaX^2 0; 0 sigmaY^2]) .* [x y]');


% Supplement your plots with a brief description of how your code works. 
% Comment on the behavior of the MAP estimate of position (visually assessed from the contour plots;
% roughly center of the innermost contour) relative to the true position. 
% Does the MAP estimate get closer to the true position as K increases? 
% Doe is get more certain? Explain how your contours justify your conclusions.

mu=pMLE;    %subjective guess at expected value for p
sigma=0.2;   %subjective guess at standard deviation around the expected value for p
alpha=mu*((mu*(1-mu)/sigma^2)-1);
beta=(1-mu)*alpha/mu;
p=linspace(0,1,1000);
prior=betapdf(p,alpha,beta);
%% 
% Construct a likelihood function for the data.

F=prod(binopdf(repmat(x',1,length(p)),repmat(n,length(x),length(p)),repmat(p,length(x),1)));
%% 
% Plot the prior
figure(2)
p1=plot(p,prior);
%% 
% Construct the marginal probability

margin=trapz(p,F.*prior);
%% 
% Construct the posterior probability and plot it

posterior=F.*prior/margin;
figure(2)
hold on
p2=plot(p,posterior);
hold off
%% 
% Compute the MAP estimate for $p$ and visualize it

[M,I]=max(posterior);
MAP=p(I)
figure(2)
hold on
plot([p(I) p(I)],[0,posterior(I)]);
text(p(I)+.05,posterior(I),strjoin({'MAP I=',num2str(p(I))}))
hold off
%% 
% Collect more data in order to compute an even more refined MAP estimate

x=[12    14     9    14];
%% 
% Update the prior to the old posterior

prior=posterior;  
%% 
% Compute a new likelihood function using the new data

F=prod(binopdf(repmat(x',1,length(p)),repmat(n,length(x),length(p)),repmat(p,length(x),1)));
%% 
% Compute the new marginal probability

margin=trapz(p,F.*prior);
%% 
% Compute the new posterior probability

posterior=F.*prior/margin;
hold on
p3=plot(p,posterior);
%% 
% Find the second, refined MAP estimate

[M,I]=max(posterior);
MAP2=p(I)
plot([p(I) p(I)],[0,posterior(I)]);
text(p(I)+.05,posterior(I),strjoin({'MAP II=',num2str(p(I))}))
legend([p1,p2,p3],'Prior','First Posterior','Second Posterior','Location','northwest')
hold off
xlabel('p')
ylabel('P(p|D) and P(p)')