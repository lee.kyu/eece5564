%%%%%%% Generate the following iid datasets %%%%%%%%
clear all;
close all;
clc;

n = 2;      % Dimensions of data
N = 20000;  % Number of data samples
classPriors = [0.6,0.4];   % Prior for label 
w1 = 0.5;   % Weight for label 0 condition 1
w2 = 0.5;   % Weight for label 0 condition 2

rng('default'); % Seed 0. Only for tests!!!

label = (rand(1,N) >= classPriors(1));  % Determine posteriors

% Create appropriate number of data points from each distrubution
N0 = length(find(label==0));
N1 = length(find(label==1));
%label = [zeros(1,N0) ones(1,N1)];

% Parameters for two classes
mu01 = [5;0]; c01 = [4 0;0 2];
mu02 = [0;4]; c02 = [1 0;0 3];
mu1 = [3;2]; c1 = [2 0;0 2];

gmmParameters.priors = [w1 w2]; % priors should be a row vector
gmmParameters.meanVectors = [mu01 mu02];
gmmParameters.covMatrices(:,:,1) = c01;
gmmParameters.covMatrices(:,:,2) = c02;
[r0, P0_label] = generateDataFromGMM(N0,gmmParameters);
r1 = mvnrnd(mu1, c1, N1)';

% Combine data into a single dataset
x = zeros(n, N);
x(:,label==0) = r0;
x(:,label==1) = r1;

% Plot data showinig two classes
figure;
plot(r0(1,:), r0(2,:), 'o');
axis equal;
hold on;
plot(r1(1,:), r1(2,:), 'x');
title('Class 0 and Class 1 True Class Labels')
xlabel('x')
ylabel('y')
legend('Class 0', 'Class 1')


%% Question 1 Part 1 %%
xT = x'
y01 = mvnpdf(xT,mu01',c01);
y02 = mvnpdf(xT,mu02',c02);
y0 = (w1*y01) + (w2*y02);
y1 = mvnpdf(xT,mu1',c1);
res = y1 ./ y0;
gamma = classPriors(1) / classPriors(2);
decision = gamma <= res;
test = [label' decision];


% Calculate discriminant scores and tau
disScore = log(mvnpdf(xT, mu1', c1) ./ (w1*mvnpdf(xT,mu01',c01) + w2*mvnpdf(xT,mu02',c02)));

% Generate vector of threshold for parmetric sweep
tau = [sort(disScore)+eps]';

% Make dicision for every threshold and calculate error values
for i = 1:length(tau)
    decision = disScore >= tau(i);
    Pfa(i) = sum(decision==1 & label'==0)/N0; % Probability of False Alarm
    Pcd(i) = sum(decision==1 & label'==1)/N1; % Probability of Correct Decision
    P_error(i) = Pfa(i) * classPriors(1) + (1-Pcd(i)) * classPriors(2);
end

%Find minimum error and corresponding threshold 
[min_error,min_index] = min(P_error); 
min_decision = (disScore >= tau(min_index)); 
min_fa = Pfa(min_index); 
min_cd = Pcd(min_index);
estimatedG = exp(tau(min_index)-eps);
  
%Find theoretical minimum error(threshold calculated using class priors) 
theo_decision = disScore >= log(classPriors(1)/classPriors(2)); 
theo_Pfa = sum(theo_decision==1 & label'==0)/N0; 
theo_Pcd = sum(theo_decision==1 & label'==1)/N1; 
theo_error = theo_Pfa*classPriors(1) +(1-theo_Pcd)*classPriors(2); 
 
%Plot ROC curve with min error point labeled 
figure(2); 
plot(Pfa,Pcd,'-',min_fa,min_cd,'o',theo_Pfa,theo_Pcd,'g+'); 
title('ROC curve of this min-P(error) classifier'); 
legend('ROC Curve','Calculated min-P(error)','Theoretical min-P(error)'); 
xlabel('False Positive Rate');
ylabel('True Positive Rate');

TP = 0, FP = 0, FN = 0, TN = 0;

for i = 1:length(test)
    if test(i, 1) == 1 & test(i, 2) == 1
        TP = TP + 1;
    elseif test(i, 1) == 1 & test(i, 2) == 0
        FN = FN + 1;
    elseif test(i, 1) == 0 & test(i, 2) == 1
        FP = FP + 1;
    else
        TN = TN + 1;
    end
end

sum = TP + FN + FP + TN;
TPper = TP / (TP + FN);
FNper = FN / (TP + FN);
FPper = FP / (FP + TN);
TNper = TN / (FP + TN);

figure;
[RocX, RocY, T, ~, OPTROCPT, SUBY, SUBYNAMES] = perfcurve(label, res, 1)
plot(RocX,RocY)
hold on
plot(OPTROCPT(1),OPTROCPT(2),'ro')
xlabel('False Positive Rate') 
ylabel('True Positive Rate')
title('ROC for Classification by likelihood-ratio')
legend('ROC Curve','optimal operating point','Location','southeast')
hold off
%% 
