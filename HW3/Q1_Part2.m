clear; close all; clc;
% GMM Algorithm for Part 2, Question 1

%% Make Synthetic Data
n = 2;      % Dimensions of data
N = 100;  % Number of data samples
classPriors = [0.6,0.4];   % Prior for label 
w1 = 0.5;   % Weight for label 0 condition 1
w2 = 0.5;   % Weight for label 0 condition 2

rng('default'); % Seed 0. Only for tests!!!

label = (rand(1,N) >= classPriors(1));  % Determine posteriors

% Create appropriate number of data points from each distrubution
N0 = length(find(label==0));
N1 = length(find(label==1));

% Parameters for two classes
mu01 = [5;0]; c01 = [4 0;0 2];
mu02 = [0;4]; c02 = [1 0;0 3];
mu1 = [3;2]; c1 = [2 0;0 2];

gmmParameters.priors = [w1 w2]; % priors should be a row vector
gmmParameters.meanVectors = [mu01 mu02];
gmmParameters.covMatrices(:,:,1) = c01;
gmmParameters.covMatrices(:,:,2) = c02;
[r0, P0_label] = generateDataFromGMM(N0,gmmParameters);
r1 = mvnrnd(mu1, c1, N1)';

% Combine data into a single dataset
x = zeros(n, N);
x(:,label==0) = r0;
x(:,label==1) = r1;


%% GMM 알고리즘 작성

clear h
my_normal = @(x, mu, sigma) 0.5*(sigma * sqrt(2*pi)) .* exp(-1 * (x-mu).^2./(2*sigma^2));

% estimate the class priors using training data in train data 10000. 
est_P_L0 = length(find(label==0)) / N;
est_P_L1 = length(find(label==1)) / N;

% As class conditional pdf models, 
% for L = 0 use a Gaussian Mixture model with 2 components, 
est_mu01 = [0 0]; est_c01 = [2 0;0 2]; % random initialization
est_mu02 = [4 4]; est_c02 = eye(2); % random initialization

% and for L = 1 use a single Gaussian pdf model. 
est_mu1 = [2 2]; est_c1 = eye(2); % random initialization

est_w = zeros(N, 3);
est_phi = [0.5, 0.5, 0.5];

xT = x';
est_y01 = mvnpdf(xT, est_mu01, est_c01);
est_y02 = mvnpdf(xT, est_mu02, est_c02);
est_y1 = mvnpdf(xT, est_mu1, est_c1);

hold on
figure(1);

h2 = plot(x, est_y02 * w2, 'xr');
h1 = plot(x, est_y01 * w1, 'ob');

legend('1st component of L = 0', '', '', 'Y 2nd component of L = 0' ,'Location','northwest');
t = text(6, 0.045, 'the first initialization','fontsize',12);
xlabel('X') 
ylabel('pdf')
title('A Gaussian Mixture model with 2 components for L = 0')
hold off

hold on
figure(2);
h3 = plot(x, est_y1, '.g');
legend('L = 1')
t = text(5, 0.045, 'the first initialization','fontsize',12);
xlabel('X') 
ylabel('pdf')
title('A single Gaussian pdf model for L = 1')
hold off

% initialize by a k-means
classes = 3;
[ labels, centroids ] = kmeans(xT, classes);
for k = 1:classes
  m{k} = centroids(k,:)';
  s{k} = cov( xT( find(labels==k), : ) );
end
% initial prior probabilities for each cluster (all equal)
phi = (ones(classes,1) * (1 / classes));

% GMM iteration
n_iter = 2;
for i_iter = 1:n_iter
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%  E-step %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    g = zeros(N, classes);
    for k=1:classes
        g(:,k) = exp(-0.5 * sum((xT-m{k}') * inv(s{k}) .* (xT-m{k}'),2)) / sqrt((2*pi)^2 * det(s{k}));
    end
    % compute the responsibility of each class producing each input data vector X belongs to each class
    % w is NxD
    w = g.*phi';
    w = w ./ sum(w,2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%% M-step %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % store the current means
    currM = m;
    % calculate the new prior probabilities based on the means
    phi = mean(w,1)';
    % Calculate the new means for each of the classes/distributions
    mm = (w'*xT)'./sum(w,1);

    for k=1:classes
      % update the mean of the class/distribution
      m{k} = mm(:,k);
      % update the covariance matrix of the class/distribution
      Xm = xT - m{k}';
      XmT = (w(:,k).*Xm)';
      test2 = sum(w);
      s{k} = (XmT*Xm) / test2(1);
    end        
end


% validation dataset D20Kvalidate
N = 20000;  % Number of data samples
classPriors = [0.6,0.4];   % Prior for label 
w1 = 0.5;   % Weight for label 0 condition 1
w2 = 0.5;   % Weight for label 0 condition 2

%rng('default'); % Seed 0. Only for tests!!!

label = (rand(1,N) >= classPriors(1));  % Determine posteriors

% Create appropriate number of data points from each distrubution
N0 = length(find(label==0));
N1 = length(find(label==1));
%label = [zeros(1,N0) ones(1,N1)];

% Parameters for two classes
mu01 = [5;0]; c01 = [4 0;0 2];
mu02 = [0;4]; c02 = [1 0;0 3];
mu1 = [3;2]; c1 = [2 0;0 2];

gmmParameters.priors = [w1 w2]; % priors should be a row vector
gmmParameters.meanVectors = [mu01 mu02];
gmmParameters.covMatrices(:,:,1) = c01;
gmmParameters.covMatrices(:,:,2) = c02;
[r0, P0_label] = generateDataFromGMM(N0,gmmParameters);
r1 = mvnrnd(mu1, c1, N1)';

% Combine data into a single dataset
x = zeros(n, N);
x(:,label==0) = r0;
x(:,label==1) = r1;
xT = x';

% Calculate discriminant scores and tau
disScore = log(mvnpdf(xT, m{3}', s{3}) ./ (w1*mvnpdf(xT,m{2}',s{2}) + w2*mvnpdf(xT,m{1}',s{1})));

% Generate vector of threshold for parmetric sweep
tau = [sort(disScore)+eps]';

% Make dicision for every threshold and calculate error values
for i = 1:length(tau)
    decision = disScore >= tau(i);
    Pfa(i) = sum(decision==1 & label'==0)/N0; % Probability of False Alarm
    Pcd(i) = sum(decision==1 & label'==1)/N1; % Probability of Correct Decision
    P_error(i) = Pfa(i) * classPriors(1) + (1-Pcd(i)) * classPriors(2);
end

%Find minimum error and corresponding threshold 
[min_error,min_index] = min(P_error); 
min_decision = (disScore >= tau(min_index)); 
min_fa = Pfa(min_index); 
min_cd = Pcd(min_index);
estimatedG = exp(tau(min_index)-eps);
  
%Find theoretical minimum error(threshold calculated using class priors) 
theo_decision = disScore >= log(classPriors(1)/classPriors(2)); 
theo_Pfa = sum(theo_decision==1 & label'==0)/N0; 
theo_Pcd = sum(theo_decision==1 & label'==1)/N1; 
theo_error = theo_Pfa*classPriors(1) +(1-theo_Pcd)*classPriors(2); 
 
%Plot ROC curve with min error point labeled 
figure(2); 
plot(Pfa,Pcd,'-',min_fa,min_cd,'o',theo_Pfa,theo_Pcd,'g+'); 
title('ROC curve of this min-P(error) classifier'); 
legend('ROC Curve','Calculated min-P(error)','Theoretical min-P(error)','Location','southeast');
text(0.2, 0.75, sprintf('the minimum probability of error = %f', min_error),'fontsize',12);
xlabel('False Positive Rate');
ylabel('True Positive Rate');
%% 

