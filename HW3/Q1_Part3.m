%% Machine Learning Online Class - Exercise 2: Logistic Regression
%
%  Instructions
%  ------------
% 
%  This file contains code that helps you get started on the second part
%  of the exercise which covers regularization with logistic regression.
%
%  You will need to complete the following functions in this exericse:
%
%     sigmoid.m
%     costFunction.m
%     predict.m
%     costFunctionReg.m
%
%  For this exercise, you will not need to change any code in this file,
%  or any other files other than those mentioned above.
%

%% Initialization
clear ; close all; clc

%% Make Synthetic Data
n = 2;      % Dimensions of data
N = 10000;  % Number of data samples
classPriors = [0.6,0.4];   % Prior for label 
w1 = 0.5;   % Weight for label 0 condition 1
w2 = 0.5;   % Weight for label 0 condition 2

rng('default'); % Seed 0. Only for tests!!!

label = (rand(1,N) >= classPriors(1));  % Determine posteriors

% Create appropriate number of data points from each distrubution
N0 = length(find(label==0));
N1 = length(find(label==1));

% Parameters for two classes
mu01 = [5;0]; c01 = [4 0;0 2];
mu02 = [0;4]; c02 = [1 0;0 3];
mu1 = [3;2]; c1 = [2 0;0 2];

gmmParameters.priors = [w1 w2]; % priors should be a row vector
gmmParameters.meanVectors = [mu01 mu02];
gmmParameters.covMatrices(:,:,1) = c01;
gmmParameters.covMatrices(:,:,2) = c02;
[r0, P0_label] = generateDataFromGMM(N0,gmmParameters);
r1 = mvnrnd(mu1, c1, N1)';

% Combine data into a single dataset
x = zeros(n, N);
x(:,label==0) = r0;
x(:,label==1) = r1;

%% Load Data
%  The first two columns contains the X values and the third column
%  contains the label (y).
X = x'; y = label';


%% =========== Part 1: Regularized Logistic Regression ============
%  In this part, you are given a dataset with data points that are not
%  linearly separable. However, you would still like to use logistic 
%  regression to classify the data points. 
%
%  To do so, you introduce more features to use -- in particular, you add
%  polynomial features to our data matrix (similar to polynomial
%  regression).
%

% Add Polynomial Features

% Note that mapFeature also adds a column of ones for us, so the intercept
% term is handled
X = mapFeature(X(:,1), X(:,2));

% Initialize fitting parameters
initial_theta = zeros(size(X, 2), 1);

% Set regularization parameter lambda to 1
lambda = 1;

% Compute and display initial cost and gradient for regularized logistic
% regression
[cost, grad] = costFunctionReg(initial_theta, X, y, lambda);

fprintf('Cost at initial theta (zeros): %f\n', cost);


%% ============= Part 2: Regularization and Accuracies =============
%  Optional Exercise:
%  In this part, you will get to try different values of lambda and 
%  see how regularization affects the decision coundart
%
%  Try the following values of lambda (0, 1, 10, 100).
%
%  How does the decision boundary change when you vary lambda? How does
%  the training set accuracy vary?
%

% Initialize fitting parameters
initial_theta = zeros(size(X, 2), 1);

% Set regularization parameter lambda to 1 (you should vary this)
lambda = 0;

% Set Options
options = optimset('GradObj', 'on', 'MaxIter', 400);

% Optimize
[theta, J, exit_flag] = fminunc(@(t)(costFunctionReg(t, X, y, lambda)), initial_theta, options);

% Plot Boundary
plotDecisionBoundary(theta, X, y);
hold on;
title(sprintf('training set sample count = %g', N))

% Labels and Legend
xlabel('X1')
ylabel('X2')

legend('label = 1', 'label = 0', 'Decision boundary')
hold off;


% validation dataset D20Kvalidate
NN = 20000;  % Number of data samples
classPriors = [0.6,0.4];   % Prior for label 
w1 = 0.5;   % Weight for label 0 condition 1
w2 = 0.5;   % Weight for label 0 condition 2

%rng('default'); % Seed 0. Only for tests!!!

label = (rand(1,NN) >= classPriors(1));  % Determine posteriors

% Create appropriate number of data points from each distrubution
N0 = length(find(label==0));
N1 = length(find(label==1));
%label = [zeros(1,N0) ones(1,N1)];

% Parameters for two classes
mu01 = [5;0]; c01 = [4 0;0 2];
mu02 = [0;4]; c02 = [1 0;0 3];
mu1 = [3;2]; c1 = [2 0;0 2];

gmmParameters.priors = [w1 w2]; % priors should be a row vector
gmmParameters.meanVectors = [mu01 mu02];
gmmParameters.covMatrices(:,:,1) = c01;
gmmParameters.covMatrices(:,:,2) = c02;
[r0, P0_label] = generateDataFromGMM(N0,gmmParameters);
r1 = mvnrnd(mu1, c1, N1)';

% Combine data into a single dataset
x = zeros(n, NN);
x(:,label==0) = r0;
x(:,label==1) = r1;
xT = x';

vaildX = mapFeature(xT(:,1), xT(:,2));
% Compute accuracy on our training set
p = predict(theta, vaildX);

fprintf('Train Accuracy: %f\n', mean(double(p == label')) * 100);
text(4.5, 7.2, sprintf('Train Accuracy: %f\n', mean(double(p == label')) * 100),'fontsize',12);

% Calculate discriminant scores and tau
disScore = log(mvnpdf(xT, mu1', c1) ./ (w1*mvnpdf(xT,mu01',c01) + w2*mvnpdf(xT,mu02',c02)));

% Generate vector of threshold for parmetric sweep
tau = [sort(disScore)+eps]';

% Make dicision for every threshold and calculate error values
for i = 1:length(tau)
    decision = disScore >= tau(i);
    Pfa(i) = sum(decision==1 & label'==0)/N0; % Probability of False Alarm
    Pcd(i) = sum(decision==1 & label'==1)/N1; % Probability of Correct Decision
    P_error(i) = Pfa(i) * classPriors(1) + (1-Pcd(i)) * classPriors(2);
end

%Find minimum error and corresponding threshold 
[min_error,min_index] = min(P_error); 
min_decision = (disScore >= tau(min_index)); 
min_fa = Pfa(min_index); 
min_cd = Pcd(min_index);
estimatedG = exp(tau(min_index)-eps);
  
%Find theoretical minimum error(threshold calculated using class priors) 
theo_decision = disScore >= log(classPriors(1)/classPriors(2)); 
theo_Pfa = sum(theo_decision==1 & label'==0)/N0; 
theo_Pcd = sum(theo_decision==1 & label'==1)/N1; 
theo_error = theo_Pfa*classPriors(1) +(1-theo_Pcd)*classPriors(2); 
 
%Plot ROC curve with min error point labeled 
figure(2); 
plot(Pfa,Pcd,'-',min_fa,min_cd,'o',theo_Pfa,theo_Pcd,'g+'); 
title(sprintf('ROC curve of min-P(error) classifier of training set %g', N)); 
legend('ROC Curve','Calculated min-P(error)','Theoretical min-P(error)'); 
xlabel('False Positive Rate');
ylabel('True Positive Rate');
text(0.2, 0.75, sprintf('the minimum probability of error = %f', min_error),'fontsize',12);


function p = predict(Theta, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

m = size(X, 1); % Number of training examples

p = zeros(m, 1);

p = sigmoid(X*Theta) >= 0.5;

end

% % Useful values
% m = size(X, 1);
% num_labels = size(Theta2, 1);
% 
% % You need to return the following variables correctly 
% p = zeros(size(X, 1), 1);
% 
% % ====================== YOUR CODE HERE ======================
% % Instructions: Complete the following code to make predictions using
% %               your learned neural network. You should set p to a 
% %               vector containing labels between 1 to num_labels.
% %
% % Hint: The max function might come in useful. In particular, the max
% %       function can also return the index of the max element, for more
% %       information see 'help max'. If your examples are in rows, then, you
% %       can use max(A, [], 2) to obtain the max for each row.
% %
% X = [ones(m, 1) X];
% 
% z2 = Theta1*X';
% 
% a2 = sigmoid(z2);
% 
% a2 = [ones(1,size(a2,2));a2];%Add a^(2)_0.
% 
% z3 = Theta2*a2;
% 
% a3 = sigmoid(z3);
% 
% [Max,p]=max(a3',[],2);%The indices of the maximum element in each row (i.e. the corresponding number) are returned to p.
% 
% % =========================================================================
% 
% end


function plotData(X, y)
%PLOTDATA Plots the data points X and y into a new figure 
%   PLOTDATA(x,y) plots the data points with + for the positive examples
%   and o for the negative examples. X is assumed to be a Mx2 matrix.

% Create New Figure
figure; hold on;

% ====================== YOUR CODE HERE ======================
% Instructions: Plot the positive and negative examples on a
%               2D plot, using the option 'k+' for the positive
%               examples and 'ko' for the negative examples.
%

% Find Indices of Positive and Negative Examples
pos = find(y==1); neg = find(y == 0);
% Plot Examples
plot(X(pos, 1), X(pos, 2), '+');
plot(X(neg, 1), X(neg, 2), 'o');


% =========================================================================

hold off;

end


function out = mapFeature(X1, X2)
% MAPFEATURE Feature mapping function to polynomial features
%
%   MAPFEATURE(X1, X2) maps the two input features
%   to quadratic features used in the regularization exercise.
%
%   Returns a new feature array with more features, comprising of 
%   X1, X2, X1.^2, X2.^2, X1*X2, X1*X2.^2, etc..
%
%   Inputs X1, X2 must be the same size
%

degree = 6;
out = ones(size(X1(:,1)));
for i = 1:degree
    for j = 0:i
        out(:, end+1) = (X1.^(i-j)).*(X2.^j);
    end
end

end



function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

J=1/m*(-y'*log(sigmoid(X*theta))-(ones(size(y))-y)'*log(ones(size(y))-sigmoid(X*theta)))+lambda/2/m*(theta'*theta-theta(1)^2);

grad(1)=1/m*(X(:,1))'*(sigmoid(X*theta)-y);

grad(2:size(theta))=1/m*(X(:,2:size(theta)))'*(sigmoid(X*theta)-y)+lambda/m*theta(2:size(theta));


% =============================================================

end


function g = sigmoid(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly 
g = zeros(size(z));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the sigmoid of each value of z (z can be a matrix,
%               vector or scalar).

g=ones(size(z))./(ones(size(z))+exp(-z));

% =============================================================

end

function plotDecisionBoundary(theta, X, y)
%PLOTDECISIONBOUNDARY Plots the data points X and y into a new figure with
%the decision boundary defined by theta
%   PLOTDECISIONBOUNDARY(theta, X,y) plots the data points with + for the 
%   positive examples and o for the negative examples. X is assumed to be 
%   a either 
%   1) Mx3 matrix, where the first column is an all-ones column for the 
%      intercept.
%   2) MxN, N>3 matrix, where the first column is all-ones

% Plot Data
plotData(X(:,2:3), y);
hold on

if size(X, 2) <= 3
    % Only need 2 points to define a line, so choose two endpoints
    plot_x = [min(X(:,2))-2,  max(X(:,2))+2];

    % Calculate the decision boundary line
    plot_y = (-1./theta(3)).*(theta(2).*plot_x + theta(1));

    % Plot, and adjust axes for better viewing
    plot(plot_x, plot_y)
    
    % Legend, specific for the exercise
    legend('Admitted', 'Not admitted', 'Decision Boundary')
    axis([30, 100, 30, 100])
else
    % Here is the grid range
    u = linspace(-3, 12, 50);
    v = linspace(-3, 12, 50);

    z = zeros(length(u), length(v));
    % Evaluate z = theta*x over the grid
    for i = 1:length(u)
        for j = 1:length(v)
            z(i,j) = mapFeature(u(i), v(j))*theta;
        end
    end
    z = z'; % important to transpose z before calling contour

    % Plot z = 0
    % Notice you need to specify the range [0, 0]
    contour(u, v, z, [0, 0], 'LineWidth', 2)
end
hold off

end





