[x, label] = generateDataQ2()
Mu = [5;5];
Mu1 = [2; 12];
CovM1 = [5 -1; -3 3];
U = rand(2, 1000);
u1 = U(:, 1:2:end);
u2 = U(:, 2:2:end);
XXX = sqrt((-2).*log(u1)) .* (cos(2*pi.*u2));
clear u1 u2 U;
[V, D] = eig(CovM1);
newX = XXX;
for j = 1 : size(XXX,2)
newX(:, j) = V * sqrt(D) * XXX(:,j);
end
newX = newX + repmat(Mu1, 1, size(newX, 2));
X1 = newX;
idx0 = 1; idx1 = 1;
newX0 = [;];
newX1 = [;];
for j = 1 : 10000 % size(label)
if label(1, j) == 0
newX0(:, idx0) = x(:, j);
idx0 = idx0 + 1;
elseif label(1, j) == 1
newX1(:, idx1) = x(:, j);
idx1 = idx1 + 1;
end
end
NewMu0 = mean(newX0')'
NewMu1 = mean(newX1')'
overallMu = (NewMu0 + NewMu1) ./ 2;
S0 = cov(newX0');
S1 = cov(newX1');
Sw = S0 + S1;
N0 = size(newX0, 2);
N1 = size(newX1, 2);
Sb0 = N0 .* (NewMu0-overallMu)*(NewMu0-overallMu)';
Sb1 = N1 .* (NewMu1-overallMu)*(NewMu1-overallMu)';
Sb = Sb0 + Sb1;
% compute the LDA projection
invSw = inv(Sw);
invSw_by_Sb = invSw * Sb;
[V, D] = eig(invSw_by_Sb);
w0 = V(:,1);
w1 = V(:,2);
hfig = figure;
axesl = axes('Parent',hfig,'FontWeight','bold','FontSize',12);
hold('all');
t = -4:10;
line_x0 = t .* w0(1);
line_y0 = t .* w0(2);
t = -4:8;
line_x1 = t .* w1(1);
line_y1 = t .* w1(2);
y0_w0 = w0'*newX0;
y1_w0 = w0'*newX1;
minY = min([min(y0_w0)], min(y1_w0));
maxY = max([max(y0_w0)], max(y1_w0));
y_w0 = minY:0.05:maxY;
y0_w0_Mu = mean(y0_w0);
y0_w0_Sigma = std(y0_w0);
y0_w0_pdf = mvnpdf(x', NewMu0', S0);
y1_w0_Mu = mean(y1_w0);
y1_w0_Sigma = std(y1_w0);
y1_w0_pdf = mvnpdf(x', NewMu1', S1);
[RocX, RocY, T, ~, OPTROCPT, SUBY, SUBYNAMES] = perfcurve(label, y1_w0_pdf, 1)
plot(RocX,RocY)
hold on
plot(OPTROCPT(1),OPTROCPT(2),'ro')
xlabel('False Positive Rate') 
ylabel('True Positive Rate')
title('ROC for Classification by LDA')
legend('ROC Curve','optimal operating point','Location','southeast')
hold off
T((RocX==OPTROCPT(1))&(RocY==OPTROCPT(2)))

function [x,labels] = generateDataQ2()
N = 10000;
figure(1)
rng('default')
class = 1:3;
classPriors = [0.3, 0.3, 0.4];
labels = randsample(class, N, true, classPriors);
for classNum = 1:3
indl = find(labels==classNum);
if classNum == 1
m1 = [4;2;1]; C1 = eye(3);
N1 = length(indl);
x(:,indl) = mvnrnd(m1,C1,N1)';
plot3(x(1,indl),x(2,indl),x(3,indl),'ro'), hold on,
axis equal,
xlabel('X'), ylabel('Y'), zlabel('Z'), grid on
elseif classNum == 2
m1 = [2;3;2]; C1 = eye(3);
N1 = length(indl);
x(:,indl) = mvnrnd(m1,C1,N1)';
plot3(x(1,indl),x(2,indl),x(3,indl),'b.'), hold on,
axis equal,
else
N0 = length(indl);
w0 = [0.5,0.5]; mu0 = [3 0 3;0 3 0;3 0 3];
Sigma0(:,:,1) = [2 0 2;0 1 0; 2 0 2]; Sigma0(:,:,2) = [2 0 0;0 2 0; 0 0 2];
gmmParameters.priors = w0;
gmmParameters.meanVectors = mu0;
gmmParameters.covMatrices = Sigma0;
[x(:,indl),components] = generateDataFromGMM(N0,gmmParameters);
plot3(x(1,indl(components==1)),x(2,indl(components==1)),x(3,indl(components==1)),'c^'), hold on, 
plot3(x(1,indl(components==2)),x(2,indl(components==2)),x(3,indl(components==2)),'g^'), hold on,
end
legend({'class 1','class 2','class 3-1', 'class 3-2'});
title('Generate 10000 samples')
end
%%%
%% 
function [x,labels] = generateDataFromGMM(N,gmmParameters)
% Generates N vector samples from the specified mixture of Gaussians
% Returns samples and their component labels
% Data dimensionality is determined by the size of mu/Sigma parameters
priors = gmmParameters.priors; % priors should be a row vector
meanVectors = gmmParameters.meanVectors;
covMatrices = gmmParameters.covMatrices;
n = size(gmmParameters.meanVectors,1); % Data dimensionality
C = length(priors); % Number of components
x = zeros(n,N); 
labels = zeros(1,N); 
% Decide randomly which samples will come from each component
u = rand(1,N); 
thresholds = [cumsum(priors),1];
for l = 1:C
indl = find(u <= thresholds(l)); 
Nl = length(indl);
labels(1,indl) = l*ones(1,Nl);
u(1,indl) = 1.1*ones(1,Nl); % these samples should not be used again
x(:,indl) = mvnrnd(meanVectors(:,l),covMatrices(:,:,l),Nl)';
end

[x, label] = generateDataQ2();
xT = x';
m_1 = [4 2 1]; 
sigma_1 = eye(3);
rng('default') % For reproducibility
yVal_1 = mvnpdf(xT,m_1,sigma_1);
m_2 = [2 3 2]; 
sigma_2 = eye(3);
yVal_2 = mvnpdf(xT,m_2,sigma_2);
m_3 = [3 0 3];
sigma_3(:,:,1) = [2 0 2;0 1 0; 2 0 2];
sigma_3(:,:,2) = [2 0 0;0 2 0; 0 0 2];
yVal_3 = mvnpdf(xT,m_3,sigma_3(:,:,1));
yVal_4 = mvnpdf(xT,m_3,sigma_3(:,:,2));
cp = [0.3, 0.3, 0.4];
label_x(:,1) = yVal_1 * cp(1);
label_x(:,2) = yVal_2 * cp(2);
label_x(:,3) = ((0.5*yVal_3) + (0.5*yVal_4)) * cp(3);
[n, decisions] = max(label_x, [], 2);
lossVal = (label == decisions');
correctIdx = find(lossVal == 1);
wrongIdx = find(lossVal == 0);
disp("correct idx count: "+ length(correctIdx))
figure
for classNum = 1:3
indl = find(label==classNum);
correct = intersect(correctIdx, indl); % gives common val and its position in 'a'
incorrect = intersect(wrongIdx, indl); % gives common val and its position in 'a'
if classNum == 1
scatter3(x(1,correct),x(2,correct),x(3,correct),'g^'), hold on,
scatter3(x(1,incorrect),x(2,incorrect),x(3,incorrect),'r^'), hold on,
elseif classNum == 2
scatter3(x(1,correct),x(2,correct),x(3,correct),'go'), hold on,
scatter3(x(1,incorrect),x(2,incorrect),x(3,incorrect),'ro'), hold on,
else
scatter3(x(1,correct),x(2,correct),x(3,correct),'g.'), hold on,
scatter3(x(1,incorrect),x(2,incorrect),x(3,incorrect),'red', '.'), hold on,
title('Minimum probability of error classification')
legend({'correct class1', 'incorrect class1','correct class2', ...
'incorrect class2','correct class3', 'incorrect class3'},'Location','southeast');
xlabel('X'), ylabel('Y'), zlabel('Z'), grid on
end
end
for t = 1:2 
loss = [0 1 10; 1 0 10; 1 1 0];
loss(:,:,2) = [0 1 100; 1 0 100; 1 1 0];
risk(:,1) = loss(1, 1, t) * label_x(:,1) + loss(1, 2, t) * label_x(:,2) + loss(1, 3, t) * label_x(:,3);
risk(:,2) = loss(2, 1, t) * label_x(:,1) + loss(2, 2, t) * label_x(:,2) + loss(2, 3, t) * label_x(:,3);
risk(:,3) = loss(3, 1, t) * label_x(:,1) + loss(3, 2, t) * label_x(:,2) + loss(3, 3, t) * label_x(:,3);
[n, decisions] = min(risk, [], 2); 
res = (label == decisions');
correctIdx = find(res == 1);
wrongIdx = find(res == 0);
disp(length(correctIdx));
figure
for l = 1:3
indl = find(label==l);
correct = intersect(correctIdx, indl); 
incorrect = intersect(wrongIdx, indl); 
if l == 1
scatter3(x(1,correct),x(2,correct),x(3,correct),'g^'), hold on,
scatter3(x(1,incorrect),x(2,incorrect),x(3,incorrect),'r^'), hold on,
elseif l == 2
scatter3(x(1,correct),x(2,correct),x(3,correct),'go'), hold on,
scatter3(x(1,incorrect),x(2,incorrect),x(3,incorrect),'ro'), hold on,
else
scatter3(x(1,correct),x(2,correct),x(3,correct),'g.'), hold on,
scatter3(x(1,incorrect),x(2,incorrect),x(3,incorrect),'red', '.'), hold on,
title('ERM classification - loss 10 times')
legend({'correct class1', 'incorrect class1','correct class2', 'incorrect class2','correct class3', 'incorrect class3'},'Location','southeast');
xlabel('X'), ylabel('Y'), zlabel('Z'), grid on
end
end
end
